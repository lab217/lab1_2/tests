from src import util

test_lst_1 = [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]
test_lst_2 = [(1, 2), (2, 0), (3, 1), (4, 10), (5, 8)]

def test_descending_order():
    result = util.shell_sort(test_lst_2, reverse=True)
    assert result == [(4, 10), (5, 8), (1, 2), (3, 1), (2, 0)]


def test_ascending_order():
    result = util.shell_sort(test_lst_2, reverse=False)
    assert result == [(2, 0), (3, 1), (1, 2), (5, 8), (4, 10)]


def test_stable_sort():
    result = util.shell_sort(test_lst_1, reverse=False)
    sorted_list = sorted(test_lst_1)
    assert util.create_dict(result) != util.create_dict(sorted_list)


def test_stable_sort_rev():
    result = util.shell_sort(test_lst_1, reverse=True)
    sorted_list = sorted(test_lst_1)
    assert util.create_dict(result) != util.create_dict(sorted_list)