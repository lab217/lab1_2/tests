from src import util


def test_correct_input_letters():
    result = util.check_input('q w e')
    assert result is False


def test_correct_input_numbers():
    result = util.check_input('9 8 32 3 5 0 1')
    assert result is True


def test_correct_input_num_with_let():
    result = util.check_input('9 83 2d s 0 1')
    assert result is False