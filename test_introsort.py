from src import util

test_lst = [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]


def test_descending_order():
    result = util.introsort(test_lst, reverse=True)
    assert result != [(7, 12), (5, 10), (2, 4), (4, 4), (7, 4), (1, 1), (3, 1), (6, 0)]


def test_ascending_order():
    result = util.introsort(test_lst, reverse=False)
    assert result != [(1, 1), (2, 4), (3, 1), (4, 4), (5, 10), (6, 0), (7, 12), (7, 4)]


def test_stable_sort():
    result = util.introsort(test_lst, reverse=False)
    sorted_list = sorted(test_lst)
    assert util.create_dict(result) == util.create_dict(sorted_list)


def test_stable_sort_rev():
    result = util.introsort(test_lst, reverse=True)
    sorted_list = sorted(test_lst)
    assert util.create_dict(result) == util.create_dict(sorted_list)